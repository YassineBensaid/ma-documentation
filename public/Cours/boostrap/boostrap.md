#*Cours de boostrap*


![](logo.png)


## *`C'est quoi le Boostrap`*
- Bootstrap est un framework qui permet de d'organiser ou d'ajouter des fonctions a celle-ci facilement et rapidement. On utilise alors des outils deja creer par d'autres personnes.

- Boostrap est une collection d'outils utiles à la création du design de sites et d'applications web .

- C'est un ensemble qui contient des codes HTML et CSS, des formulaires,boutons, outils de navigation et autres élements interactifs. 

##*`Les balises de referencement`*

- Les balise de referencement fonctionne comme des < div > mais elles servent aux moteurs de recherche.
Si l'on met plusieurs balise de referencement comme section le moteur de recherche va penser que l'on traite de X sujet different.


< section > < /section >


- section permet de disposer le site en plusieurs categories aidant au referencement.


< header > < /header >

header un titre ou un paragraphe explicatif ou informatif du sujet.


< footer > < /footer >

footer permet de rediriger vers une autre page ou bien vers une autre partie de la page ou du site. Mais permet de sité les sources et autres informations.

##*`Utilisation`*

Pour commencer il faut visiter le site : https://getbootstrap.com/docs/4.3/getting-started/introduction/
Une fois sur le site Bootstrap, on a deux versions une avec seulement le CSS et l'autre avec du JavaScript.

![](boost.png)

##*`col et row`*
- `col` et `row` permettent de diviser les espaces de la page en ligne (row) et en colonne (col), chaque ligne et chaque colonne sont séparer par de petit espaces pour les distinguer.
  Un row doit etre toujours suivi d'un col
- `col` peut aller jusqu'a 12 maximum