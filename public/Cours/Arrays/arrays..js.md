#Arrays

![](manejo-de-arrays-en-javascript-t2.jpg)

## *c'est quoi `array`*

- array est un tableau en anglais
- array est une variable unique qui est utilisée pour stocker différents éléments comme des nombres, des strings, des objets...

## *Les opérateurs*
- La méthode `sort()` trie les éléments d'un tableau, dans ce même tableau, et renvoie le tableau

- La méthode `map()` crée un nouveau tableau avec les résultats de l'appel d'une fonction fournie sur chaque élément du tableau appelant.

- La méthode `forEach()` permet d'exécuter une fonction donnée sur chaque élément du tableau.

- La méthode `filter()` crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine  

- La méthode `every()` permet de tester si tous les éléments d'un tableau vérifient une condition donnée par une fonction en argument. Cette méthode renvoie un booléen pour le résultat du test

- La méthode `some()` teste si au moins un élément du tableau passe le test implémenté par la fonction fournie. Elle renvoie un booléen indiquant le résultat du test.
