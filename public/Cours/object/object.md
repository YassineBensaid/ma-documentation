#Cours de les objets

##*c'est quoi les objets*
- Un `objet` est un ensemble de propriétés et une propriété est une association entre un nom (aussi appelé clé) et une valeur.

- on peut utiliser `object.keys` et `object.value`.

###*Transformer des clés d'un objet en tableau:*
on utilise `object.keys`
![](clé_tableau.png)
###*Modifier les clés d'un tableau*:
pour modifier les clés de le tableau on utilise `map()`
![](keys.PNG)
###*Transformation des valeurs d'un objet en tableau*:
on utilise `object.value`
![](tableauValues.PNG)
###*Modifier les valeurs d'un tableau*:
![](valeur_tableau.PNG)