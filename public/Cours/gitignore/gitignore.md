#Cours de gitignore

## *c'est quoi le gitgnore*
- gitignore = Spécifie les fichiers intentionnellement non suivis à ignorer
- Les fichiers ignorés sont généralement des artefacts de compilation et des fichiers générés par la machine qui peuvent être dérivés des sources de votre référentiel ou qui ne devraient pas être livrés

**exmple:**
le dossier `.idea`:
- C'est un dossier qui contient tous les fichiers de configuration de notre naviagateur webstorm
  le point devant veut dire que c'est un fichier caché donc n'apparaitra pas ct Files et les fichiers cachés sont en coul

##*Comment paramétrer le navigateur pour guider les fichiers cachés*

- On doit  créer un fichier qui s'appelle `gitignore`
![](Annotation%202019-09-30%20174316.png)
- Si on ne veut pas envoyer un fichier sur Gilab on le saisit dans le fichier **.gitignore** .
- Dans le cas d'un fichier que l'on veut ne pas montrer mais qui est déjà paramétrer automatiquement en "add to GIT",
  on doit forcer manuellement.
  Pour cela on ouvre la console en bas puis on saisit "GIT rm + le nom du fichier" avec la mention "-f"(-force).
