#Cours de Markdown

![Markdown]( imgmd.jpg)

## *`C'est quoi le Markdown`*
 - *Mardownn est un langage de mise en forme de texte simple et un langage de balisage.Un document balisé par Markdown peut etre converti en HTML.Les fichiers de ce langage enregistrés au format .md*


##*`Git et Gitlab`*


###*Git* : 

![](git-logo.png)

Git est un protocole qui permet de stocker les information.

###*Gitlab* :   
![gitlub](stacked_wm_no_bg.png)

Gitlab et Github sont des sites internet qui utilisent le protocole Git.

##*`Comment stocker MD avec Gitlab`*

- Créer un nouveau projet dans gitlab
- Mettre le projet en pubic el l'initialise 
- Sélectionner le lien clone avec HTTPS et lr copier sur un nouveau projet sur WebStorm
- pour envoyer un fichier ou dossier sur GitLab on effectue un `commit`, le commit permet de choisir les fichiers, dossiers et autres a selectionner pour l'envoie
- Suite au `commit` on `push` qui permet de valider l'envoie vers GitLab

##*`Comment structurer ma page`*

- La structure d'une page doit etre organiser en plusieur partie elle meme avec des sous partie.

 - Une page internet doit comporter uniquement un seul H1 le titre le plus important de la page qui va porter sur le sujet de la page.
Les balise H2 H3 H4 ont une ordre d'importance qu'il faut respecter.
Organiser les differentes partie avec leurs contenue et leurs sujets (comme une dissertation).
L'organisation est super importante car elle permet un meilleur SEO (Search Engine Optimization), meilleur référencement sur internet.

##*`vocabulaire`*

###*`WisWig`*
 - Nous permettons de faire la mise en forme en html.
Editeur WisWig est un editeur qui nous permettons d'editer directement dans le web de contenue dans nos navigateurs.

###*`Les Backticks`*
- (alt gr+7) C'est pour écrire sous la forme d'un code

####*Backtick simple*

 - Pour l'utiliser on met une backtick avant la phrase et une a la fin de celle-ci.
AltGr + 7 = backtick sur Windows
cette phrase est importante

####*Backtick triple*

 - Pour l'utiliser on met 3 backticks avant le code en question et 3 a la fin.
Apres les 3 backticks on choisit le type de code (JS, C , etc...).

### *`SEO`*
 - SEO est l'acronyme de **Search Engine Optimization** et peut etre défini comme l'art de positionner un site,une page web ou une application.




